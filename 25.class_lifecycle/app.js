class SimpleHeader extends React.Component{
    timer = null;
    componentDidMount(){
        console.log("The header component was just mounted");
        this.timer = setInterval(()=>{
            console.log(`Running this after every 1 second ${Date.now()}`);
        },1000);
    }
    componentWillUnmount(){
        console.log("The header component will be unmounted");
        clearInterval(this.timer);
    }
    render(){
        return (
            <h1>
                Simple Header Component
            </h1>
        )
    }
}

class App extends React.Component{
    state = {
        showHeader: false
    }
    toggleShowHeader =()=>{
        this.setState({
            showHeader:!this.state.showHeader
        });
    }
    componentDidUpdate(){
        console.log("The app component was just updated");
    }
    render(){
        return (<div>
                <div className="simplebutton" onClick={this.toggleShowHeader}>
                    {this.state.showHeader?"Hide":"Show"} Header
                </div>
                {this.state.showHeader?<SimpleHeader/>:null}
            </div>)
    }
}




ReactDOM.render(<App/>,document.querySelector('.app'));