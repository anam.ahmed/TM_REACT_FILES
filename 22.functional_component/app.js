function HomeComponent(props){
    return (<div>
        <h1>Hello {props.name}</h1>
    </div>);
}

ReactDOM.render(<HomeComponent name="TechMasters"/>,document.querySelector('.app'));