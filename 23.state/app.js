class HomeComponent extends React.Component{
    state={
        name:"anam",
        age: 50
    }
    changeName = (e) =>{
        let newName = e.target.value;
        this.setState({
            name : newName,
            age  : Math.random()
        });
    }
    render(){
        return (<div>
            <input type="text" onKeyPress={this.changeName}/>
            <h1>Hello {this.state.name}, your age is {this.state.age}</h1>
        </div>);
    }
}

ReactDOM.render(<HomeComponent/>,document.querySelector('.app'));